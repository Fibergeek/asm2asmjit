import re

MNEM_CORRECTIONS = {
  'pushfw': 'pushf',
  'popfw': 'popf',
  'pushaw': 'pusha',
  'popaw': 'popa',
  'xor': 'xor_',
  'or': 'or_',
  'and': 'and_',
  'not': 'not_',
}
MEM_SIZE = [
  'byte ptr ',
  'dword ptr ',
  'qword ptr ',
  'tword ptr ',
  'oword ptr ',
  'dqword ptr ',
  'qqword ptr ',
  'xmmword ptr ',
  'ymmword ptr ',
  'zmmword ptr ',
  'word ptr ',
]

class BaseConvertor:
 def convert(self, code):
  lines_with_comments = list(self.parse_code_into_lines_with_comments(code))
  labels = list(self.get_initial_labels())
  labels.extend(self.convert_lines_with_comments(lines_with_comments, None))
  #print('Labels:', repr(labels))
  if labels:
    for line in self.convert_labels(labels):
      yield line
    yield ''
  for line in self.convert_lines_with_comments(lines_with_comments, labels):
    yield line

 def get_initial_labels(self):
  return []

 def convert_labels(self, labels):
  for label in labels:
    yield 'Label lbl_' + label + ' = cb.newLabel();'

 def parse_code_into_lines_with_comments(self, code):
  for line in code.split('\n'):
    code = code.replace('\t', ' ')
    
    if ';' in line:
      line, _, comment = line.partition(';')
      comment = comment.strip()
    else:
      comment = None
    
    while ': ' in line:
      line = line.replace(': ', ':')
    
    if '::' in line:
      raise ValueError('Invalid syntax')
    
    line = line.strip()
    yield line, comment

 def convert_lines_with_comments(self, lines_with_comments, labels):
  for line, comment in lines_with_comments:
    if ' ' in line:
      mnem, _, operands = line.partition(' ')

      # Don't process the operands if we just want the labels
      if labels != None:
        operands = ', '.join([self.convert_op_to_asmjit(op, labels) for op in operands.split(',')])
      else:
        operands = None
    else:
      mnem = line
      operands = ''

    while True:
      if not ':' in mnem:
        break
      label, _, mnem = mnem.partition(':')
      label = label.strip()
      if labels == None:
        yield label
      elif comment and not mnem:
        yield 'cb.bind(lbl_' + label + '); // ' + comment
        comment = ''
      else:
        yield 'cb.bind(lbl_' + label + ');'
    
    # Don't do any further processing if we just want the labels
    if labels == None:
      continue
    
    if mnem:
      mnem = mnem.lower()
      mnem = MNEM_CORRECTIONS.get(mnem, mnem)
      asmjit = 'cb.' + mnem + '(' + operands + ');'
    else:
      asmjit = ''
    
    if comment:
      if asmjit:
        asmjit += ' '
      asmjit += '// ' + comment

    if asmjit:
      yield asmjit

 def convert_op_to_asmjit(self, op, labels):
  op = self.unify_spaces(op)

  # TODO: cs: is IDA only
  if 'cs:' in op and not '[' in op and not ']' in op:
    new_op = op.replace('cs:', 'cs:[') + ']'
    #print('Fixing IDA RIP op : {0} -> {1}'.format(repr(op), repr(new_op)))
    op = new_op
  if 'ds:' in op and not '[' in op and not ']' in op:
    new_op = op.replace('ds:', 'ds:[') + ']'
    #print('Fixing IDA RIP op : {0} -> {1}'.format(repr(op), repr(new_op)))
    op = new_op
  if 'ss:' in op and not '[' in op and not ']' in op:
    new_op = op.replace('ss:', 'ss:[') + ']'
    #print('Fixing IDA RIP op : {0} -> {1}'.format(repr(op), repr(new_op)))
    op = new_op
  if 'es:' in op and not '[' in op and not ']' in op:
    new_op = op.replace('es:', 'es:[') + ']'
    #print('Fixing IDA RIP op : {0} -> {1}'.format(repr(op), repr(new_op)))
    op = new_op
  if 'fs:' in op and not '[' in op and not ']' in op:
    new_op = op.replace('fs:', 'fs:[') + ']'
    #print('Fixing IDA RIP op : {0} -> {1}'.format(repr(op), repr(new_op)))
    op = new_op
  if 'gs:' in op and not '[' in op and not ']' in op:
    new_op = op.replace('gs:', 'gs:[') + ']'
    #print('Fixing IDA RIP op : {0} -> {1}'.format(repr(op), repr(new_op)))
    op = new_op

  if '0[' in op:
    new_op = op.replace('0[', '[0+')
    #print('Fixing IDA RIP op : {0} -> {1}'.format(repr(op), repr(new_op)))
    op = new_op
  if '0Fh[' in op:
    new_op = op.replace('0Fh[', '[0x0F+')
    #print('Fixing IDA RIP op : {0} -> {1}'.format(repr(op), repr(new_op)))
    op = new_op
  if '1[' in op:
    new_op = op.replace('1[', '[1+')
    #print('Fixing IDA RIP op : {0} -> {1}'.format(repr(op), repr(new_op)))
    op = new_op
  
  if ':' in op:
    seg, _, op = op.partition(':')
    if ' ' in seg:
      non_seg, _, seg = seg.rpartition(' ')
      op = non_seg + ' ' + op
    seg = seg.lower()
  else:
    seg = ''
  
  if self.is_constant(op):
    # Assume the operand is a constant
    return 'Imm(' + self.convert_constant(op) + ')'
  
  if '[' in op or ']' in op:
    return self.convert_memop_to_asmjit(op, seg, labels)
  
  if self.is_label(op, labels):
    return self.convert_label(op)
  
  return op.lower()

 def convert_memop_to_asmjit(self, op, seg, labels):
  for mem_size in MEM_SIZE:
    if mem_size in op.lower():
      op = re.sub(mem_size, '', op, flags=re.I)
      mem_size = mem_size.split(' ')[0] + '_'
      break
  else:
    mem_size = ''
  
  #print('INPUT', op)
  if op.startswith('[') and op.endswith(']'):
    # Assume the operand is a memory access
    op = op[1:-1]
    label_reg, base_reg, index_reg, index_mul, constants = self.parse_memop(op, labels)
    
    # Build an AsmJit ptr
    if label_reg:
      if index_reg or index_mul:
        # NOTE: AsmJit does not support a label with an index register specification
        mem = 'ptr(' + label_reg + ', ' + base_reg + ' + ' + index_reg + ' * ' + index_mul + ', ' + constants + ')'
      elif base_reg:
        mem = 'ptr(' + label_reg + ', ' + base_reg + ', ' + constants + ')'
      else:
        mem = 'ptr(' + label_reg + ', ' + constants + ')'
    elif index_reg or index_mul:
      if not base_reg:
        base_reg = '0'
      if index_reg and index_mul == '1':
        mem = 'ptr(' + base_reg + ', ' + index_reg + ', 0, ' + constants + ')' 
      elif index_reg and index_mul == '2':
        mem = 'ptr(' + base_reg + ', ' + index_reg + ', 1, ' + constants + ')'
      elif index_reg and index_mul == '4':
        mem = 'ptr(' + base_reg + ', ' + index_reg + ', 2, ' + constants + ')'
      elif index_reg and index_mul == '8':
        mem = 'ptr(' + base_reg + ', ' + index_reg + ', 3, ' + constants + ')'
      else:
        # NOTE: the index multiplier is probably incorrect
        mem = 'ptr(' + base_reg + ', ' + str(index_reg) + ', ' + str(index_mul) + ', ' + constants + ')'
    else:
      mem = 'ptr(' + base_reg + ', ' + constants + ')'
    
    mem = mem_size + mem.replace('(, ', '(').replace(', )', ')')
    
    if seg:
      return 'withSegment(' + mem + ', ' + seg + ')'
    #print('RESULT', mem)
    return mem
  
  # We were unable to convert this! - Return as-is
  return op

 def parse_memop(self, op, labels):
  base_regs, index_reg, index_mul, constants = [], None, None, ''
  op, oper = op.replace(' ', ''), '+'
  
  debug = False and 'rsp-' in op

  while op:
    pos = re.search(r'[+-]', op[1:])
    pos = -1 if not pos else pos.start(0)

    if pos >= 0:
      if oper == '-':
        part, oper, op = op.partition(op[pos + 1])
        part = '-' + part
      else:
        part, oper, op = op.partition(op[pos + 1])
    else:
      part, op = op, ''
      
    if debug:
      print(op, '-->', part, '-->', oper, '==>', constants)

    if self.is_constant(part) and not '*' in part:
      if constants:
        constants += ' ' + oper + ' ' + self.convert_constant(part)
      elif oper != '+':
        constants = oper + self.convert_constant(part)
      else:
        constants = self.convert_constant(part)
    else:
      # This is fucked up, for exmaple [r8-8]
      base_regs.append(part)

  if debug:
    print('==>', constants)

  for i, base_reg in enumerate(base_regs):
    if '*' in base_reg:
      left, multiply, right = base_reg.partition('*')
      if self.is_constant(left) and self.is_constant(right):
        continue
      if self.is_constant(left) and not self.is_constant(right):
        index_mul, index_reg = left, right
        base_regs = base_regs[:i] + base_regs[i+1:]
        break
      if not self.is_constant(left) and self.is_constant(right):
        index_mul, index_reg = right, left
        base_regs = base_regs[:i] + base_regs[i+1:]
        break

  label_regs, base_regs = self.split_into_labels_and_others(base_regs, labels)
  if len(base_regs) >= 2 and not index_reg and not index_mul:
     index_mul = '1'
     index_reg = base_regs[-1]
     base_regs = base_regs[:-1]
  
  return ' + '.join(label_regs), ' + '.join(base_regs), index_reg, index_mul, constants

 def split_into_labels_and_others(self, data, labels):
  ret_labels, ret_others = [], []
  for i, s in enumerate(data):
    if self.is_label(s, labels):
      ret_labels.append(self.convert_label(s))
    else:
      ret_others.append(s.lower())
  return ret_labels, ret_others

 def unify_spaces(self, s):
  return ' '.join(s.split())

 def convert_label(self, op):
  return 'lbl_' + op

 def is_label(self, op, labels):
  return op in labels

 def convert_constant(self, op):
  return op

 def is_constant(self, op):
  if not op:
    return False
  c = op[0]
  return c.isdigit() or c == '+' or c == '-'

