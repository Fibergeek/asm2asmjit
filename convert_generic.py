from convert import BaseConvertor

def convert_asm_to_asmjit(code):
    for line in BaseConvertor().convert(code):
        yield line
