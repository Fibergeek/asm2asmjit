from ida_struct import get_struc_size, get_member_by_name, get_struc
from ida_funcs import get_func

#from convert import BaseConvertor

CONSTANT_CHARS = '0123456789+-'

class IdaConvertor(BaseConvertor):
    def __init__(self, local_names, stack_names):
        self.ida_names = list(Names())
        self.local_names = list(local_names)
        self.stack_names = dict(stack_names)
        self.seen_names = set()

    def convert(self, code):
        for stack_name, soffs in self.stack_names.items():
            yield 'constexpr auto ' + stack_name + ' = ' + str(soffs) + ';'
        if self.stack_names:
            yield ''
        items = []
        for item in BaseConvertor.convert(self, code):
            items.append(item)
        for seen_name in self.seen_names:
            ea = get_name_ea(BADADDR, seen_name)
            if ea == BADADDR:
                raise ValueError('Unable to lookup "{0}"'.format(seen_name))
            yield 'constexpr auto ' + correct_ida_name(seen_name) + ' = ' + hex(ea) + ';'
        if self.seen_names:
            yield ''
        for item in items:
            yield item

    def is_ida_name(self, x):
        #result = [1 for ea, name in self.ida_names if name==x ]
        result = get_name_ea(BADADDR, x) != BADADDR
        #print('is_ida_name {0} : {1}'.format(x, result))
        #if result:
        #    return True
        #print('  result {0} : {1}'.format(x, result))
        return result

    def convert_label(self, op):
        #print('convert_label({0})'.format(repr(op)))
        if op.startswith('short '):
            op = op[6:]
        return 'lbl_' + op
    
    def is_label(self, op, labels):
        #print('is_label({0})'.format(repr(op)))
        if op.startswith('short '):
            op = op[6:]
        result = op in labels   # -->  this actually checks against GLOBALS or self.is_ida_name(op)
        #print('  result {0} : {1}'.format(op, result))
        return result
    
    def convert_constant(self, op):
        #print('convert_constant({0})'.format(repr(op)))
        if op.startswith('0x') or op.startswith('-0x'):
          return op
        if op in self.stack_names:
            return op
        if op.find('+') > 0:
            remainder = op[op.index('+'):]
            op = op[:op.index('+')]
        else:
            remainder = ''
        if self.is_ida_name(op):
            self.seen_names.add(op)
            op = correct_ida_name(op)
            return 'relocated(' + op + remainder + ')'
        if remainder:
            raise NotImplementedError('We have remainder? : {0}'.format(remainder))
        if op.endswith('h'):
          if op.startswith('-'):
            return '-0x' + op[1:-1]
          return '0x' + op[:-1]
        if op.endswith('b'):
          if op.startswith('-'):
            return '-' + hex(int(op[1:-1], 2))
          return hex(int(op[:-1], 2))
        return str(int(op))
        raise NotImplementedError('convert_constant({0})'.format(repr(op)))
    
    def is_constant(self, op):
        #print('is_constant({0})'.format(repr(op)))
        if op.find('+') > 0:
            op = op[:op.index('+')]
        result = op[0] in CONSTANT_CHARS or op in self.stack_names or (self.is_ida_name(op) and not op in self.local_names)
        #print('  result {0} : {1}'.format(op, result))
        return result

def correct_ida_name(name):
    return name.replace('?', '_').replace('@', '_')

def get_stack_names(ea=None):
    ea = get_screen_ea() if ea == None else ea
    
    frame = ida_frame.get_frame(ea)
    frame = ida_funcs.get_func(ea)
    
    offs_sr = ida_frame.frame_off_savregs(frame)
    offs_args = ida_frame.frame_off_args(frame)
    offs_lvars = ida_frame.frame_off_lvars(frame)
    offs_retaddr = ida_frame.frame_off_retaddr(frame)
    
    diff = offs_args - offs_sr  
    
    # find the stack frame
    stack = get_frame_id(ea)
    if stack == None:
      return []
    size  = get_struc_size(stack)
    adjuster = size - (size - offs_retaddr)
    if get_func_flags(ea) & FUNC_FRAME:
      #print('WARNING FRAME:', hex(offs_sr), hex(offs_args), hex(offs_lvars), hex(offs_retaddr), hex(get_struc_size(get_frame_id(ea))))
      # TODO: this this on 1s6-bit/32-bit systems
      # TODO: what if BP==SP ?
      adjuster -= 8
    adjuster = offs_sr

    # figure out all of the variable names
    # SEE : https://gist.github.com/syndrowm/2968620
    # TODO: add support for 32-bit EBP frames
    names = {}
    for i in range(size):
        n = get_member_name(stack, i)
        if n:
            names[n.strip()] = get_member_by_name(get_struc(stack), n).soff - adjuster
    
    return names

if False:
  # Test get_stack_names
  print('\n'.join(map(lambda n: n[0] + ': ' + hex(n[1]), get_stack_names().items())))

def get_func_labels(ea=None):
    ea = get_screen_ea() if ea == None else ea
    
    if ea != BADADDR:
        for item in FuncItems(ea):
            name = get_name(item)
            if name:
                yield name

def convert_asm_to_asmjit(code_or_ea_or_func):
    from idautils import FuncItems

    if isinstance(code_or_ea_or_func, int):
      ea = code_or_ea_or_func
      labels = []
      code = get_disasm_for_asmjit(ea)
    elif isinstance(code_or_ea_or_func, str):
      ea = BADADDR
      labels = []
      code = code_or_ea_or_func
    else:
      ea = code_or_ea_or_func.start_ea
      labels = get_func_labels(ea)
      lines = []
      for item_ea in FuncItems(ea):
        name = get_name(item_ea)
        if name:
          lines.append(name + ':')
        line = get_disasm_for_asmjit(item_ea)
        if 'lock ' in line:
          lines.append('lock')
          line = line.replace('lock ', '') # TODO: fix this hack
        lines.append(line)
      code = '\n'.join(lines)

    for line in IdaConvertor(labels, get_stack_names(ea)).convert(code):
        yield line

def get_disasm_for_asmjit(item_ea):
    from idautils import DecodeInstruction
    from idc import GetDisasm

    code = GetDisasm(item_ea)

    if not 'ptr ' in code:
        inst = DecodeInstruction(item_ea)
        if is_any_mem_op(inst):
            op_len = op_count(inst)
            if op_len == 1:
                left, _, right = code.partition(' ')
                old, code = code, left + ' ' + get_op_size_name(inst.ops[0]) + ' ptr ' + right
                #print('We (signle) fixed {0} into {1}'.format(old, code))
            elif op_len == 2 and is_any_imm_op(inst):
                left, _, right = code.partition(' ')
                old, code = code, left + ' ' + get_op_size_name(inst.ops[0]) + ' ptr ' + right
                #print('We (imm) fixed {0} into {1}'.format(old, code))
            elif op_len == 2 and get_op_size(inst.ops[0]) != get_op_size(inst.ops[1]):
                if is_mem_op(inst.ops[0]) and not is_mem_op(inst.ops[1]):
                    left, _, right = code.partition(' ')
                    old, code = code, left + ' ' + get_op_size_name(inst.ops[0]) + ' ptr ' + right
                    #print('We (left) fixed {0} into {1}'.format(old, code))
                elif is_mem_op(inst.ops[1]) and not is_mem_op(inst.ops[0]):
                    left, _, right = code.partition(',')
                    old, code = code, left + ', ' + get_op_size_name(inst.ops[1]) + ' ptr ' + right.lstrip()
                    #print('We (right) fixed {0} into {1}'.format(old, code))
                else:
                    raise NotImplementedError('Unable to fix "{0}"'.format(code))
    
    if code.startswith('db ') or code.startswith('dw ') or code.startswith('dd ') or code.startswith('dq '):
        raise NotImplementedError('Starting with ' + code)
        
    if ('rbp-' in code or 'rsp-' in code) and ('+var_' in code.lower() or '+arg_' in code.lower()):
        print('STRONG WARNING RELATED TO STACK:', code)
    if ('-var_' in code.lower() or '-arg_' in code.lower()):
        raise NotImplementedError('Minus has not been tested yet in this situation!')
    #elif 'p-' in code or 'i-' in code or 'x-' in code or 'd-' in code or 'r8-' in code or 'r9-' in code or 'r10-' in code or 'r11-' in code or 'r12-' in code or 'r13-' in code or 'r14-' in code or 'r15-' in code:
    #    print('STRONG WARNING RELATED TO MINUS:', code)

    return code

def op_count(inst):
    for count,op in enumerate(inst):
        if op.type == idaapi.o_void:
            return count
    return count

def get_op_size_name(op):
    size = get_op_size(op)
    if size == 1:
        return 'byte'
    if size == 2:
        return 'word'
    if size == 4:
        return 'dword'
    if size == 8:
        return 'qword'
    #if size == 16:
    #    return 'dqword'
    raise ValueError('Not yet supported : {0}'.format(size))

def get_op_size(op):
    from ida_ua import get_dtype_size
    return get_dtype_size(op.dtype)

def is_mem_op(op):
    return is_op_of_type(op, (idc.o_mem, idc.o_phrase, idc.o_displ))

def is_any_imm_op(inst):
    return is_any_op_of_type(inst, idc.o_imm)

def is_any_mem_op(inst):
    return is_any_op_of_type(inst, (idc.o_mem, idc.o_phrase, idc.o_displ))

def is_any_op_of_type(inst, expected_type):
    for op in inst.ops:
        if is_op_of_type(op, expected_type):
            return True
    return False

def is_op_of_type(op, expected_type):
    if isinstance(expected_type, int):
        return op.type == expected_type
    for type in expected_type:
        if is_op_of_type(op, type):
            return True
    return False

def convert_function_to_asmjit(ea):
    ea = get_screen_ea() if ea == None else ea

    func = get_func(ea)
    return 'void ' + get_func_name(ea) + '(Builder &cb)\n{\n  ' + '\n  '.join(convert_asm_to_asmjit(func)) + '\n}\n'

def export_workflow_funcs_as_asmjit(out_file, include_restored=False):
    from myprew import get_verified_funcs, get_restored_funcs
    to_dump = list(get_verified_funcs())
    if include_restored:
        to_dump.extend(get_restored_funcs())
    for result in export_funcs_as_asmjit(out_file, to_dump):
        yield result

def export_funcs_as_asmjit(out_file, funcs):
    template = """
#include <asmjit/asmjit.h>

using namespace asmjit;
using namespace asmjit::x86;

constexpr auto OriginalImageBase = 0x140000000;
uintptr_t FunctionBuildersImageBase;

__forceinline uintptr_t relocated(uintptr_t base, uintptr_t offset=0)
{
    return base - OriginalImageBase + FunctionBuildersImageBase + offset;
}

Mem &withSegment(const Mem &mem, const SReg &seg_reg)
{
    Mem copy(mem);
    copy.setSegment(seg_reg);
    return copy;
}
"""
    
    out_file.write(template.strip() + '\n\n')
    to_dump = funcs if isinstance(funcs, list) else list(funcs)
    for func_ea in to_dump:
        print('Exporting ' + hex(func_ea))
        for line in convert_function_to_asmjit(func_ea).split('\n'):
            line = line.replace('cb.retn', 'cb.ret')
            if 'cb.lea(' in line:
                line = line.replace('Imm', 'ptr')
            if 'cb.rep(' in line:
                line = line.replace(');', '();').replace('.rep(', '.rep(); cb.')
            if 'cb.cmpltps(' in line:
                line = line.replace(');', ', 1);').replace('.cmpltps(', '.cmpps(')
            if 'cb.cmpleps(' in line:
                line = line.replace(');', ', 2);').replace('.cmpleps(', '.cmpps(')
            if not 'cb.nop(' in line:
                out_file.write(line + '\n')
        yield func_ea

def export_workflow_funcs_to_templated_file(filename, write_prebuilder=True, include_restored=False):
    pre_template = """#include <Prebuilding.h>

static Instrumentation::FunctionInfo const FunctionInfos[]{
"""
    post_template = """
};
"""
    prebuilder_template = """
const Instrumentation::FunctionInfo *GetPrebuilder(const uintptr_t branchVA)
{
    auto img = Instrumentation::Image::findEnclosing(branchVA);

    if (img != nullptr)
    {
        auto rva = branchVA - img->base;
        for (int i = 0; i < sizeof(FunctionInfos) / sizeof(FunctionInfos[0]); i ++)
        {
            if (FunctionInfos[i].rva == rva)
                return &FunctionInfos[i];
            if (FunctionInfos[i].rva > rva)
                break;
        }
    }
    return nullptr;
}
"""
    import ida_bytes
    with open(filename, 'wt') as f:
        target_funcs = {}
        for func_ea in export_workflow_funcs_as_asmjit(f, include_restored):
            if ida_bytes.get_original_byte(func_ea) != 0xE9:
                raise NotImplementedError('Expected 0xE9 at {0}'.format(func_ea))
            target_funcs[func_ea + ida_bytes.get_original_dword(func_ea + 1) + 5] = func_ea
        lines = []
        for intercepted_ea, func_ea in sorted(target_funcs.items()):
            #lines.append("""    {{ {0} - OriginalImageBase, {1} - OriginalImageBase, {2} }},""".format(hex(intercepted_ea), hex(func_ea), get_func_name(func_ea)))
            lines.append("""    {{ {0} - OriginalImageBase, {2} }}, // {1}""".format(hex(intercepted_ea), hex(func_ea), get_func_name(func_ea)))
        f.write(pre_template)
        f.write('\n'.join(lines))
        f.write(post_template)
        if write_prebuilder:
          f.write(prebuilder_template)

# USAGE: export_workflow_funcs_to_templated_file(filename, write_prebuilder=?, include_restored=?)
