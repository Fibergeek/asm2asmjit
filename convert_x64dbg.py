from convert import BaseConvertor

UPPERCASE_HEX  = '0123456789ABCDEF'
CONSTANT_CHARS = UPPERCASE_HEX + '+-'

class x64dbgConvertor(BaseConvertor):
    def convert_label(self, op):
        if op[0] == '<' and op[-1] == '>':
          return op[1:-1]
        return 'lbl_' + op
    
    def is_label(self, op, labels):
        return (op in labels) or (op[0] == '<' and op[-1] == '>')
    
    def convert_constant(self, op):
        if op.startswith('0x'):
          return op
        return '0x' + op
    
    def is_constant(self, op):
        return op[0] in CONSTANT_CHARS

def convert_asm_to_asmjit(code):
    for line in x64dbgConvertor().convert(code):
        yield line
