import unittest
import inspect

from convert_generic import convert_asm_to_asmjit

class TestStringMethods(unittest.TestCase):
    def setUp(self):
        self.maxDiff = None
    
    def assertConversion(self):
        outer_frame = inspect.currentframe().f_back
        caller_name = inspect.getframeinfo(inspect.currentframe().f_back).function
        caller_doc = eval('self.' + caller_name + '.__doc__')
        input, expected = [strip_lines(s) for s in caller_doc.split('--->')]
      
        output = strip_lines('\n'.join(convert_asm_to_asmjit(input)))
        self.assertEqual(output.split('\n'), expected.split('\n'))

    def test_mnemonics_only(self):
        """
        nop
        cbw
        --->
        cb.nop();
        cb.cbw();
        """
        self.assertConversion()

    def test_mnemonics_with_one_register(self):
        """
        inc rax
        inc        rax
        neg al
        --->
        cb.inc(rax);
        cb.inc(rax);
        cb.neg(al);
        """
        self.assertConversion()

    def test_mnemonics_with_two_registers(self):
        """
        mov rax, r15
        sub al, bl
        --->
        cb.mov(rax, r15);
        cb.sub(al, bl);
        """
        self.assertConversion()

    def test_mnemonics_with_one_register_and_constant(self):
        """
        mov rax, 0xffff
        sub al, -1
        --->
        cb.mov(rax, Imm(0xffff));
        cb.sub(al, Imm(-1));
        """
        self.assertConversion()

    def test_mnemonics_with_three_operands(self):
        """
        imul rdx, rax, 3
        imul    rdx   ,    rax     ,    3
        --->
        cb.imul(rdx, rax, Imm(3));
        cb.imul(rdx, rax, Imm(3));
        """
        self.assertConversion()

    def test_mnemonics_with_only_constants(self):
        """
        push 1
        push -1
        push 0xff
        --->
        cb.push(Imm(1));
        cb.push(Imm(-1));
        cb.push(Imm(0xff));
        """
        self.assertConversion()

    def test_mnemonics_with_one_mem_operands(self):
        """
        inc byte ptr [eax]
        inc  byte  ptr  [rax]
        inc  word ptr [ebx]
        inc dword ptr [rcx]
        inc qword ptr [rdx]
        --->
        cb.inc(byte_ptr(eax));
        cb.inc(byte_ptr(rax));
        cb.inc(word_ptr(ebx));
        cb.inc(dword_ptr(rcx));
        cb.inc(qword_ptr(rdx));
        """
        self.assertConversion()

    def test_mem_operands_with_constants(self):
        """
        mov al, [eax+1]
        mov al, [1+eax]
        mov al, [eax+1+1]
        mov al, [0+eax]
        mov al, [-1+eax]
        mov al, [eax+0]
        mov al, [eax-1]
        mov al, [0+eax-1]
        mov al, [0x01+eax-0x01]
        --->
        cb.mov(al, ptr(eax, 1));
        cb.mov(al, ptr(eax, 1));
        cb.mov(al, ptr(eax, 1 + 1));
        cb.mov(al, ptr(eax, 0));
        cb.mov(al, ptr(eax, -1));
        cb.mov(al, ptr(eax, 0));
        cb.mov(al, ptr(eax, -1));
        cb.mov(al, ptr(eax, 0 - 1));
        cb.mov(al, ptr(eax, 0x01 - 0x01));
        """
        self.assertConversion()

    def test_mem_operands_with_two_registers(self):
        """
        mov al, [eax+ebx]
        mov al, [ebx+eax]
        mov al, [1+eax+ebx-1]
        --->
        cb.mov(al, ptr(eax, ebx, 0));
        cb.mov(al, ptr(ebx, eax, 0));
        cb.mov(al, ptr(eax, ebx, 0, 1 - 1));
        """
        self.assertConversion()

    def test_mem_operands_with_an_index_register(self):
        """
        mov al, [eax*8]
        mov al, [eax+ebx*4]
        mov al, [2*eax+ebx]
        mov al, [255+2*eax+ebx-256]
        --->
        cb.mov(al, ptr(0, eax, 3));
        cb.mov(al, ptr(eax, ebx, 2));
        cb.mov(al, ptr(ebx, eax, 1));
        cb.mov(al, ptr(ebx, eax, 1, 255 - 256));
        """
        self.assertConversion()

    def test_invalid_mem_operands(self):
        """
        mov al, [-eax]
        mov al, [1-eax]
        --->
        """
        pass#self.assertConversion()

    def test_mnemonics_with_irregular_stack_size(self):
        """
        pushfw
        popfw
        pushaw
        popaw
        --->
        cb.pushf();
        cb.popf();
        cb.pusha();
        cb.popa();
        """
        self.assertConversion()

    def test_logical_mnemonics(self):
        """
        xor al, al
        and al, al
        or al, al
        not al
        --->
        cb.xor_(al, al);
        cb.and_(al, al);
        cb.or_(al, al);
        cb.not_(al);
        """
        self.assertConversion()

    def test_comments(self):
        """
        ; comment at the start of a line
            ; comment with spaces in front the start
        nop ; comment after an instruction
        nop      ; comment   with   spaces
        --->
        // comment at the start of a line
        // comment with spaces in front the start
        cb.nop(); // comment after an instruction
        cb.nop(); // comment   with   spaces
        """
        self.assertConversion()

    def test_backward_label(self):
        """
        a:
        jmp a
        b:
        lea rax, [b]
        c:
        lea rcx, [c + 0xFF]
        lea rcx, [c + 0xFF + rcx]
        lea rcx, [c + rcx + 0xFF]
        lea rcx, [rcx + c + 0xFF]
        lea rcx, [rcx + 0xFF + c]
        --->
        Label lbl_a;
        Label lbl_b;
        Label lbl_c;
        
        cb.bind(lbl_a);
        cb.jmp(lbl_a);
        cb.bind(lbl_b);
        cb.lea(rax, ptr(lbl_b));
        cb.bind(lbl_c);
        cb.lea(rcx, ptr(lbl_c, 0xFF));
        cb.lea(rcx, ptr(lbl_c, rcx, 0xFF));
        cb.lea(rcx, ptr(lbl_c, rcx, 0xFF));
        cb.lea(rcx, ptr(lbl_c, rcx, 0xFF));
        cb.lea(rcx, ptr(lbl_c, rcx, 0xFF));
        """
        self.assertConversion()

    def test_forward_label(self):
        """
        call a
        lea eax, [a]
        a:
        --->
        Label lbl_a;
        
        cb.call(lbl_a);
        cb.lea(eax, ptr(lbl_a));
        cb.bind(lbl_a);
        """
        self.assertConversion()

    def test_multiple_labels_on_same_line(self):
        """
        a:b:c:nop
        d:e:
        --->
        Label lbl_a;
        Label lbl_b;
        Label lbl_c;
        Label lbl_d;
        Label lbl_e;

        cb.bind(lbl_a);
        cb.bind(lbl_b);
        cb.bind(lbl_c);
        cb.nop();
        cb.bind(lbl_d);
        cb.bind(lbl_e);
        """
        self.assertConversion()

    def test_labels_with_comments(self):
        """
        a: ; one label, one comment
        b:c: ; two labels, one comment
        d:e:nop ; two labels, one instruction, one comment
        --->
        Label lbl_a;
        Label lbl_b;
        Label lbl_c;
        Label lbl_d;
        Label lbl_e;

        cb.bind(lbl_a); // one label, one comment
        cb.bind(lbl_b);
        cb.bind(lbl_c); // two labels, one comment
        cb.bind(lbl_d);
        cb.bind(lbl_e);
        cb.nop(); // two labels, one instruction, one comment
        """
        self.assertConversion()

def strip_lines(s):
    return '\n'.join([s.strip() for s in s.split('\n')]).strip()

if __name__ == '__main__':
    unittest.main()
