import unittest
import inspect

from convert_x64dbg import convert_asm_to_asmjit

class TestStringMethods(unittest.TestCase):
    def setUp(self):
        self.maxDiff = None
    
    def assertConversion(self):
        outer_frame = inspect.currentframe().f_back
        caller_name = inspect.getframeinfo(inspect.currentframe().f_back).function
        caller_doc = eval('self.' + caller_name + '.__doc__')
        input, expected = [strip_lines(s) for s in caller_doc.split('--->')]
        
        output = strip_lines('\n'.join(convert_asm_to_asmjit(input)))
        self.assertEqual(output.split('\n'), expected.split('\n'))
    
    def test_constants(self):
        """
        mov al, 0
        mov al, 0x0
        mov al, A0
        mov al, 0xA0
        --->
        cb.mov(al, Imm(0x0));
        cb.mov(al, Imm(0x0));
        cb.mov(al, Imm(0xA0));
        cb.mov(al, Imm(0xA0));
        """
        self.assertConversion()
    
    def test_labels(self):
        """
        jmp qword ptr ds:[<&TerminateProcess>]
        jmp image.label
        --->
        cb.jmp(withSegment(qword_ptr(&TerminateProcess), ds));
        cb.jmp(image.label);
        """
        self.assertConversion()
    
    def test_memory_operands(self):
        """
        mov rcx,qword ptr ds:[<__security_cookie>]
        mov qword ptr ss:[rsp+rax+20],rcx
        --->
        cb.mov(rcx, withSegment(qword_ptr(__security_cookie), ds));
        cb.mov(withSegment(qword_ptr(rsp, rax, 0, 0x20), ss), rcx);
        """
        self.assertConversion()
    
    def test_uppercase_listings(self):
        """
        LEA RCX,QWORD PTR SS:[RSP+40]
        CALL QWORD PTR DS:[<&UnhandledExceptionFilter>]
        --->
        cb.lea(rcx, withSegment(qword_ptr(rsp, 0x40), ss));
        cb.call(withSegment(qword_ptr(&UnhandledExceptionFilter), ds));
        """
        self.assertConversion()

def strip_lines(s):
    return '\n'.join([s.strip() for s in s.split('\n')]).strip()

if __name__ == '__main__':
    unittest.main()
